'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService, BerriesService, $q) {
    $scope.listsLoaded=false;

    $q.all([PokemonsService.getPokemons(), BerriesService.getBerries()]).
    then(function(response) {
        $scope.pokemons = response[0].data.results;
        $scope.berries = response[1].data.results;
        $scope.listsLoaded=true;
    });


});
