'use strict';

pokemonApp.controller('EditPokemonCtrl', function($scope, PokemonsService, $routeParams) {

  PokemonsService.getPokemon($routeParams['pokemonId']).then(function(response) {
      $scope.editPokemonData = response.data;
      $scope.pokemonLoaded = true;
  });

    $scope.editPokemon = function(myPokemon) {

        $scope.editSuccess = false;
        PokemonsService.editPokemon(myPokemon).then(function(response) {

            $scope.newPokemon = {};

            $scope.newPokemonId = response.data.objectId;
            $scope.editSuccess = true;

        });


    }

});
