angular
    .module('PokemonApp')
    .factory('PokemonsService', function($http) {

            return {

                getPokemons: function() {
                    return $http.get('http://pokeapi.co/api/v2/pokemon/?limit=10');
                },

                getPokemon: function(pokemonId) {
                    return $http.get('http://pokeapi.co/api/v2/pokemon/' + pokemonId);
                },

                createPokemon: function(pokemonData) {
                    return $http({
                        method: 'POST',
                        url: 'https://api.backendless.com/v1/data/pokemon',
                        headers: {
                            "application-id": "8110BBA4-9A1E-DB1E-FF0E-37689A7B4C00",
                            "secret-key": "83B4C68B-C3E2-827F-FFD8-593CCB146600"
                        },
                        data: pokemonData
                    });
                },
                editPokemon: function(pokemonData) {
                    return $http({
                        method: 'PUT',
                        url: 'https://api.backendless.com/v1/data/pokemon',
                        headers: {
                            "application-id": "8110BBA4-9A1E-DB1E-FF0E-37689A7B4C00",
                            "secret-key": "83B4C68B-C3E2-827F-FFD8-593CCB146600"
                        },
                        data: pokemonData
                    });
                },
                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: 'https://api.backendless.com/v1/data/pokemon/' + pokemonId,
                        headers: {
                            "application-id": "8110BBA4-9A1E-DB1E-FF0E-37689A7B4C00",
                            "secret-key": "83B4C68B-C3E2-827F-FFD8-593CCB146600"
                        }
                    });
                }

            }

        }

    );
